#coding: utf-8
from socket import *
import sys
import re
#using the socket module


if len(sys.argv) != 2:
    exit("need port number")

serverPort = int(sys.argv[1]) 


# with socketserver.TCPServer(("", serverPort), SimpleHTTPRequestHandler) as httpd:
#     httpd.serve_forever()


serverSocket = socket(AF_INET, SOCK_STREAM)
# serverSocket.settimeout(5)
#This line creates the server’s socket. The first parameter indicates the address family; in particular,AF_INET indicates that the underlying network is using IPv4.The second parameter indicates that the socket is of type SOCK_STREAM,which means it is a TCP socket (rather than a UDP socket, where we use SOCK_DGRAM).

serverSocket.bind(('localhost', serverPort))
#The above line binds (that is, assigns) the port number 12000 to the server’s socket. In this manner, when anyone sends a packet to port 12000 at the IP address of the server (localhost in this case), that packet will be directed to this socket.

serverSocket.listen(1)
#The serverSocket then goes in the listen state to listen for client connection requests. 

print("============== The server is ready ==============")

while 1:
    connectionSocket, addr = serverSocket.accept()
#When a client knocks on this door, the program invokes the accept( ) method for serverSocket, which creates a new socket in the server, called connectionSocket, dedicated to this particular client. The client and server then complete the handshaking, creating a TCP connection between the client’s clientSocket and the server’s connectionSocket. With the TCP connection established, the client and server can now send bytes to each other over the connection. With TCP, all bytes sent from one side not are not only guaranteed to arrive at the other side but also guaranteed to arrive in order

    #wait for data to arrive from the client
    request = connectionSocket.recv(1024).decode('utf-8')

    print(request)

    try:
        request = request.split()[1].split("/")[1]
    except:
        # not request
        continue
    # render html 
    try:
        required_file = open(request, 'rb')
        # parse the file name 
        if bool(re.search('.png', request)):
            header = b"image/png"
        else:
            header = b"text/html"
        connectionSocket.send(b'HTTP/1.1 200 OK\r\n')
        connectionSocket.send(b"Content-Type: " + header +b"\r\n\r\n")
        connectionSocket.send(required_file.read())
    except:
        connectionSocket.send(b'HTTP/1.1 404 Not Found\r\n')
        connectionSocket.send(b"Content-Type: text/html\r\n\r\n")
        connectionSocket.send(b'<html><body><h1>404 NOT FOUND</body></html>')


    #and send it back to client
    connectionSocket.close()

