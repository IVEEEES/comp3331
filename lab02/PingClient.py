#coding: utf-8
from socket import *
import time
import sys

if len(sys.argv) != 3:
    exit("need server name and port number")

#Define connection (socket) parameters
#Address + Port no
#Server would be running on the same host as Client
serverName = sys.argv[1]
#change this port number if required
serverPort = int(sys.argv[2])

clientSocket = socket(AF_INET, SOCK_DGRAM)
clientSocket.settimeout(0.5)
#This line creates the client’s socket. The first parameter indicates the address family; 
# in particular,AF_INET indicates that the underlying network is using IPv4.The second parameter indicates that the socket is of type SOCK_DGRAM,
# which means it is a UDP socket (rather than a TCP socket, where we use SOCK_STREAM).

for i in range(0,10):
    
    message = "PING " + str(i) + " " + str(time.ctime()) + " \r\n"
    # time sent
    sendTime = time.time()
    clientSocket.sendto(message.encode('utf-8'), (serverName, serverPort))
    # Note the difference between UDP sendto() and TCP send() calls. In TCP we do not need to attach the destination address to the packet, while in UDP we explicilty specify the destination address + Port No for each message

    # receive message from server, buffer size is 2048 bytes
    try:
        modifiedMessage, serverAddress = clientSocket.recvfrom(2048)# Note the difference between UDP recvfrom() and TCP recv() calls.
        rtt = time.time() - sendTime
        # print(modifiedMessage.decode('utf-8'))
        print("ping to 127.0.0.1, seq %d, rtt = %d ms"%(i, round(rtt * 1000)))
    except:
        # timeout
        print("ping to 127.0.0.1, seq %d, rtt = time out"%(i))

clientSocket.close()
# Close the socket
