import sys
import re
import time
from socket import *
from Graph import Graph

ROUTE_UPDATE_INTERVAL  = 30
UPDATE_INTERVAL = 1

if __name__ == "__main__":
    # try to read the file
    if len(sys.argv) != 2:
        exit("need config file")

    # read config
    try:
        config = open(sys.argv[1], "r").read()
        # remove the last line
        lines = config.split("\n")
        # read the information form line
        name = lines[0].split(" ")[0]
        portNum = int(lines[0].split(" ")[1])
        numNeighbours = int(lines[1])        
    except:
        exit("cannot read config file")

    # initialise edges of neighbours
    edgeList = []
    for index, line in enumerate(lines[2:-1]):
        print(line.split(" ")[0])
        edgeList.append(name + " " + line)

    print(edgeList)


    # start server
    serverSocket = socket(AF_INET, SOCK_DGRAM)
    serverSocket.bind(('localhost', portNum))
    serverSocket.settimeout(0.5)
    print("server %s started"%(name))

    sep = ", "
    while 1:
        print("----------------------------")
        # send topology to others
        for index, line in enumerate(lines[2:-1]):
            serverSocket.sendto(sep.join(edgeList).encode('utf-8'), ("localhost", int(line.split(" ")[2])))
            print("Sending " + sep.join(edgeList))
        
        # receive from others

        # send the numNeighbours, if different form last time, update
        while(1):
            try:
                request, clientAddress = serverSocket.recvfrom(2048)
                # check duplicates and if 
                # TODO: the better ways is to check the num neighbours in every 0.5 seconds
                # for newEdge in request.split("\n"):
                #     if not newEdge in edgeList:
                #         edgeList.append(newEdge)
                print("Receiving " + request)
            except:
                # nothing to receive 
                break
                
        time.sleep(UPDATE_INTERVAL)

    # update link state information every 1s

    # after 30 sefcond run dijkstra to map current routes
    
    
    serverSocket.close()